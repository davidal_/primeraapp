import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-empleado',
  templateUrl: './empleado.component.html',
  styleUrls: ['./empleado.component.css']

})
export class EmpleadoComponent implements OnInit {

nombre = "David";
apellido = "Alonso";
edad=24;
empresa = "Compusoft";


/*getEdad(){
  return this.edad;
}*/

habilitacionCuadro = true;
usuRegistrado = false;
textoDeRegistro = "No hay nadie registrado";


getRegistroUsuario(){
  this.usuRegistrado = false;
}

setusuarioRegistrado(event:Event){
  //alert("El usuario se acaba de registrar");
  if((<HTMLInputElement>event.target).value=="si"){
    this.textoDeRegistro = "El usuario se acaba de registrar";
  }else{
    this.textoDeRegistro = "No hay nadie registrado";
  }
  
  
}



constructor() { }

ngOnInit(): void {
}

}
